#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Title
# GNU Radio version: 3.8.1.0

import signal
import sys
import time
import math

import osmosdr
from gnuradio import analog
from gnuradio import gr

import visa


class hackrfSignalGenerator(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Title")

        ##################################################
        # Variables
        ##################################################
        self.vga_gain = vga_gain = 47
        self.samp_rate = samp_rate = 20e6
        self.freq = freq = 100e6
        self.amp = amp = True

        ##################################################
        # Blocks
        ##################################################
        self.osmosdr_sink_0 = osmosdr.sink(
            args="numchan=" + str(1) + " " + ""
        )
        self.osmosdr_sink_0.set_time_unknown_pps(osmosdr.time_spec_t())
        self.osmosdr_sink_0.set_sample_rate(samp_rate)
        self.osmosdr_sink_0.set_center_freq(freq, 0)
        self.osmosdr_sink_0.set_freq_corr(0, 0)
        self.osmosdr_sink_0.set_gain(amp, 0)
        self.osmosdr_sink_0.set_if_gain(vga_gain, 0)
        self.osmosdr_sink_0.set_bb_gain(0, 0)
        self.osmosdr_sink_0.set_antenna('', 0)
        self.osmosdr_sink_0.set_bandwidth(0, 0)
        self.analog_sig_source_x_0 = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, 1e3, 127, 0, 0)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_sig_source_x_0, 0), (self.osmosdr_sink_0, 0))


    def get_vga_gain(self):
        return self.vga_gain

    def set_vga_gain(self, vga_gain):
        self.vga_gain = vga_gain
        self.osmosdr_sink_0.set_if_gain(self.vga_gain, 0)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.osmosdr_sink_0.set_sample_rate(self.samp_rate)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.osmosdr_sink_0.set_center_freq(self.freq, 0)

    def get_amp(self):
        return self.amp

    def set_amp(self, amp):
        self.amp = amp
        if amp:
            self.osmosdr_sink_0.set_gain(14, 0)
        else:
            self.osmosdr_sink_0.set_gain(0, 0)


class MeasurementDevice:
    def __init__(self, addr):
        self.resource_manager = visa.ResourceManager("@py")
        self.instr = self.resource_manager.open_resource(addr)
        if self.instr is None:
            print("Error: Instrument with VISA address {} could not be opened!".format(addr))
            exit(1)
        self.instr.timeout = 10000

    #def __del__(self):
        #self.instr.clear()
        #self.instr.close()

    def list_resources(self):
        return self.resource_manager.list_resources()

    def get_dbm(self):
        pass


class Oscilloscope(MeasurementDevice):
    def __init__(self, addr):
        MeasurementDevice.__init__(self, addr)
        self.enable_sequential()
        self.instr.write(":CHANNEL1:RANGE {}".format(8 * 40e-3))
        self.instr.write("CHANNEL1:COUPLING DCFIFTY; :TRIG:LEV 0; :TRIG:SOURCE CHANNEL1; :TIM:MODE AUTO")

    def get_dbm(self):
        self.digitize()
        vtop = float(self.instr.query(":MEAS:SCRATCH;:MEAS:VTOP?"))
        if vtop <= 0:
            print("VTop invalid! Exiting.")
            exit()
        dbm = 10 + 20 * math.log10(vtop)
        return dbm

    def get_dbm_realtime(self):
        self.disable_sequential()
        dbm = self.get_dbm()
        self.enable_sequential()
        return dbm

    def adjust_timebase_to_freq(self, freq):
        period = 1 / freq
        self.instr.write(":TIMEBASE:RANGE {}".format(8 * period))

    def enable_sequential(self):
        # Setup sequential single shot for averaging
        points = 512
        segments = 4
        self.instr.write("CHANNEL1:COUPLING DCFIFTY; :TRIG:LEV 0; :TRIG:SOURCE CHAN1; :TIM:MODE AUTO")
        self.instr.write(":TIM:SAMP REAL")
        self.instr.write(":SEQ:DISP ON")
        self.instr.write(":ACQ:TYPE AVER")
        self.instr.write(":ACQ:COUNT {}".format(segments))
        self.instr.write(":SEQ:NPO {}; :SEQ:NSEG {}".format(points, segments))
        self.instr.write(":SEQ:SOURCE CHAN1")

    def disable_sequential(self):
        self.instr.write(":SEQ:DISP OFF")

    def autoscale(self):
        self.instr.write(":AUTOSCALE")

    def digitize(self):
        self.instr.write(":DIGITIZE CHANNEL1")

    def reset(self):
        self.instr.write("*RST")


class SpectrumAnalyzer(MeasurementDevice):
    def __init__(self, addr):
        MeasurementDevice.__init__(self, addr)

def dbm_distance(gain_tuple, dbm_wanted):
    return abs(dbm_wanted - gain_tuple[1])

def main(top_block_cls=hackrfSignalGenerator, options=None):
    hackrf = top_block_cls()
    instr = Oscilloscope("GPIB0::7::INSTR")

    def sig_handler(sig=None, frame=None):
        hackrf.stop()
        hackrf.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    csv = open("out.csv", "w")
    csv.write("Frequency [MHz],RF [dB],IF [dB],Measured output power [dBm]\n")

    hackrf.start()

    # For low frequencies, the amp introduces lots of distortion!
    # However, it is needed for high frequencies
    prefer_amp = False
    dBm_wanted = -10

    for freq in range(int(10e6), int(900e6), int(10e6)):
        hackrf.set_freq(freq)
        instr.adjust_timebase_to_freq(freq)
        time.sleep(0.05)

        # Check whether enabling the amp is possible.
        if prefer_amp:
            hackrf.set_amp(True)
            hackrf.set_vga_gain(0)
            time.sleep(0.1)
            amp_enable = True if instr.get_dbm() <= dBm_wanted else False
        # Check whether enabling the amp is necessary.
        else:
            hackrf.set_amp(False)
            hackrf.set_vga_gain(47)
            time.sleep(0.1)
            amp_enable = False
            amp_enable = False if instr.get_dbm() >= dBm_wanted else True

        hackrf.set_amp(amp_enable)

        # Start in the middle
        gain = round(47 / 2)
        gain_candidates = []
        for iteration in range(2, 8):
            hackrf.set_vga_gain(gain)
            time.sleep(0.05)
            dbm = instr.get_dbm()
            #print("VGA gain: {} --> {:.2f} dBm".format(gain, dbm))

            if iteration > 3:
                gain_candidates.append((gain, dbm))

            if dbm > dBm_wanted:
                gain -= round(47 / pow(2, iteration))
            else:
                gain += round(47 / pow(2, iteration))

        result = min(gain_candidates, key=lambda g: dbm_distance(g, dBm_wanted))

        csv.write("{},{},{},{:.2f}\n".format(freq/1e6, "14" if amp_enable else "0", result[0], result[1]))
        csv.flush()

        print("Amp {} + VGA gain: {} for {:.2f} dBm at {} MHz".format("enabled" if amp_enable else "disabled", result[0], result[1], freq/1e6))

    hackrf.stop()
    hackrf.wait()


if __name__ == '__main__':
    main()
